/******************************************************************************
 * e-Stick CDC Sensor Data Generator                                          *
 *                                                                            *
 * Author: Christian Fibich <fibich@technikum-wien.at>                        *
 * Last Change: 2015-03-12                                                    *
 ******************************************************************************/

/******************************************************************************/
/* Original license header                                                    */
/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/
/******************************************************************************/

/** \file
 *
 *  Header file for eCDC.c.
 */

#ifndef _ECDC_H_
#define _ECDC_H_

	/* Includes: */
		#include <avr/io.h>
		#include <avr/wdt.h>
		#include <avr/power.h>
		#include <avr/interrupt.h>
		#include <string.h>
		#include <stdio.h>

		#include "Descriptors.h"

		#include <LUFA/Drivers/USB/USB.h>
		#include <LUFA/Platform/Platform.h>

		#include <stdlib.h>
		#include <math.h>

	/* Function Prototypes: */
		void SetupHardware(void);

		void EVENT_USB_Device_Connect(void);
		void EVENT_USB_Device_Disconnect(void);
		void EVENT_USB_Device_ConfigurationChanged(void);
		void EVENT_USB_Device_ControlRequest(void);

		int getline(USB_ClassInfo_CDC_Device_t * pcdc, char *msg, int bufsize);

	/* Macros */
		#define MAX_MSG_SIZE        80
		#define IS_MSG_TERM(c)      (((c) == '\n') || ((c) == '\r'))

		#define PARSE_HAVE_DATA     1
		#define PARSE_NO_DATA       2
		#define PARSE_ERR           255
        
        #define CMD_READ            'r'
        #define CMD_WRITE           'w'
        
        #define CMD_ACK             'a'
        #define CMD_NACK            'n'

#endif