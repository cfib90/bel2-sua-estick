/******************************************************************************
 * e-Stick CDC Sensor Data Generator                                          *
 *                                                                            *
 * Author: Christian Fibich <fibich@technikum-wien.at>                        *
 * Last Change: 2015-04-08                                                    *
 ******************************************************************************/

/******************************************************************************/
/* Original license header                                                    */
/*
             LUFA Library
     Copyright (C) Dean Camera, 2014.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2014  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaims all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/
/******************************************************************************/

/** \file
 *
 *  Main source file for e-Stick data generator module. This file contains the 
 *  main tasks and is responsible for the initial application hardware configuration.
 */

#include "eCDC.h"

static char msg[MAX_MSG_SIZE];

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *  passed to all CDC Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface = {
    .Config = {
        .ControlInterfaceNumber = INTERFACE_ID_CDC_CCI,
        .DataINEndpoint = {
            .Address = CDC_TX_EPADDR,
            .Size = CDC_TXRX_EPSIZE,
            .Banks = 1,
        },
        .DataOUTEndpoint = {
            .Address = CDC_RX_EPADDR,
            .Size = CDC_TXRX_EPSIZE,
            .Banks = 1,
        },
        .NotificationEndpoint = {
            .Address = CDC_NOTIFICATION_EPADDR,
            .Size = CDC_NOTIFICATION_EPSIZE,
            .Banks = 1,
        },
    },
};

/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *  used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;


/** Get a valid line from the CDC serial interface
 *  A valid line is at most as long as the buffer size - 1 and ends with a
 *  termination character as defined in the macro IS_MSG_TERM()
 */
int getline(USB_ClassInfo_CDC_Device_t * pcdc, char *msg, int bufsize) {
    static uint8_t bytes_read = 0;
    int16_t bytes_to_read;
    uint8_t i;
    int rval;
    char recv;

    rval = -1;

    bytes_to_read = CDC_Device_BytesReceived(pcdc);

    while ((bytes_to_read--) > 0) {

        if (bytes_read == 0) {
            for (i = 0; i < bufsize; i++)
                msg[i] = 0;
        }
        recv = CDC_Device_ReceiveByte
               (&VirtualSerial_CDC_Interface) & 0xFF;

        if (bytes_read < bufsize - 1) {
            msg[bytes_read] = recv;
            if (IS_MSG_TERM(recv)) {
                /* msg done */
                msg[bytes_read + 1] = '\0';
                rval = bytes_read;
                bytes_read = 0;
                break;
            } else {
                bytes_read++;
            }
        } else if (IS_MSG_TERM(recv)) {
            /* when message is longer than buffer,
             * ignore until next message terminator
             */
            bytes_read = 0;
        }
    }
    return rval;
}

/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
int main(void) {
    uint8_t parse;
    unsigned int src,dst,id,address,data;
    char cmd,scmd;
    unsigned int sine_freq;
    unsigned int sine_ampl;
    unsigned int sine_x;
    unsigned int tri_freq;
    unsigned int tri_ampl;
    unsigned int tri_x;
    unsigned int rnd_ampl;

    /* Default values for functions */
    sine_freq = 1;
    sine_ampl = 0x7FFF;
    sine_x    = 0;
    tri_freq  = 1;
    tri_ampl  = 0x7FFF;
    tri_x     = 0;
    rnd_ampl  = 0xFFFF;

    /* Call setup function to initialize hardware */
    SetupHardware();

    /* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
    CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

    /* Enable global interrupt */
    GlobalInterruptEnable();

    while(1) {
        /* get a valid line from the buffer */
        if (getline(&VirtualSerial_CDC_Interface, msg, MAX_MSG_SIZE) > 0) {
            if (sscanf(msg, "<%u,%u,%u,%c,%u,%u>", &src, &dst, &id, &cmd, &address, &data) == 6) {
                /* Received command and data are valid */
                parse = PARSE_HAVE_DATA;
            } else if(sscanf(msg, "<%u,%u,%u,%c,%u>", &src, &dst, &id, &cmd, &address) == 5) {
                /* Received command id valid, no data attached */
                data  = 0;
                parse = PARSE_NO_DATA;
            } else {
                /* Received command is invalid */
                parse = PARSE_ERR;
            }
            
            if (parse == PARSE_ERR) {
                /* set source and destination to some kind of default config */
                src  = 65535;
                dst  = 65535;
                id   = 65535;
                scmd = CMD_NACK;
            } else {
                /* Truncate bits left of 2^15 */
                src &= 0xFFFF;
                dst &= 0xFFFF;
                id  &= 0xFFFF;
                address &= 0xFFFF;
                data    &= 0xFFFF;
    
                /* Initially set reply to ACKnowledge */
                scmd = CMD_ACK;

                if ((cmd == CMD_WRITE) && (parse == PARSE_HAVE_DATA)) {
                    /* Write registers */
                    switch(address) {
                        case 0: {
                            /* Set LEDs (PORT B) directly */
                            PORTB = data & 0xFF;
                        }
                        break;
                        case 1: {
                            /* Set LEDs (PORT B) via AND connection */
                            PORTB &= (data & 0xFF);
                        }
                        break;
                        case 2: {
                            /* Set LEDs (PORT B) via OR connection */
                            PORTB |= (data & 0xFF);
                        }
                        break;
                        case 3: {
                            /* Set LEDs (PORT B) via XOR connection */
                            PORTB ^= (data & 0xFF);
                        } break;
                        case 10: {
                            /* Set frequency of sine function */
                            sine_freq = data;
                        }
                        break;
                        case 11: {
                            /* Set amplitude of sine function */
                            sine_ampl = (data > 0x7FFF) ? 0x7FFF : data;
                        }
                        break;
                        case 12: {
                            /* Reset sine function */
                            sine_x = 0;
                        }
                        break;
                        case 20: {
                            /* Set frequency of triangle function */
                            tri_freq = data;
                        }
                        break;
                        case 21: {
                            /* Set amplitude of triangle function */
                            tri_ampl = (data > 0x7FFF) ? 0x7FFF : data;
                        }
                        break;
                        case 22: {
                            /* Reset triangle function */
                            tri_x = 0;
                        }
                        break;
                        case 31: {
                            /* Set amplitude of random number generating function */
                            rnd_ampl = data;
                        }
                        break;
                        case 32: {
                            /* Set seed of random number generating function */
                            srand(data);
                        }
                        break;
                        default:
                            /* Invalid address */
                            scmd = CMD_NACK;
                    }
                } else if (cmd == CMD_READ) {
                    /* Read registers */
                    switch(address) {
                        case 0: {
                            /* Get PINB (LEDs) */
                            data = PINB;
                        }
                        break;
                        case 1: {
                            /* Get PINC */
                            data = PINC;
                        }
                        break;
                        case 2: {
                            /* Get PIND */
                            data = PIND;
                        }
                        break;
                        case 3: {
                            /* Get SREG */
                            data = SREG;
                        }
                        break;
                        case 10: {
                            /* Get sine value */
                            /* value = amp * sin(2*PI*freq*t) + offset */
                            data = (unsigned int)(sine_ampl*sin(((double)sine_freq)*((double)sine_x/0xFFFF)*2*M_PI)+0x7FFF);
                            sine_x = (sine_x+1) % 0xFFFF;
                        }
                        break;
                        case 20: {
                            /* Get triangle value */
                            if(tri_x < 0x4000) {
                                data = (unsigned int)(tri_ampl*((double)tri_x/0x4000)) + 0x7FFF;
                            } else if(tri_x < 0x8000) {
                                data = (unsigned int)(tri_ampl*((0x8000-((double)tri_x))/0x4000))+0x7FFF;
                            } else if(tri_x < 0xC000) {
                                data = (unsigned int)(tri_ampl*((0xC000-((double)tri_x))/0x4000))+(0x7FFF-tri_ampl);
                            } else {
                                data = (unsigned int)(tri_ampl*(((double)tri_x-0xC000)/0x4000))+(0x7FFF-tri_ampl);
                            }
                            tri_x += tri_freq % 0xFFFF; 
                        }
                        break;
                        case 30: {
                            /* random */
                            data = (unsigned int)(rnd_ampl*((double)rand()/RAND_MAX));
                        }
                        break;
                        default:
                            /* Invalid address */
                            scmd = CMD_NACK;
                    }
                } else {
                    /* Invalid command */
                    scmd = CMD_NACK;
                }
            }
            
            if(scmd == 'a') {
                /* make sure data is no longer than 16 bit */
                data &= 0xFFFF;
                /* Reply with ACK command */
                (void) fprintf(&USBSerialStream,"<%u,%u,%u,%c,%u>\n",dst,src,id,scmd,data);
            } else {
                /* Reply with NACK command */
                (void) fprintf(&USBSerialStream,"<%u,%u,%u,%c>\n",dst,src,id,scmd);
            }
        }
        /* Call required LUFA functions */
        CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
        USB_USBTask();
    }
}

/** Configures the board hardware and chip peripherals */
void SetupHardware(void) {
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    /* LEDs                                       */
    DDRB = 0xFF;        /* set portB as output    */
    /* Latch                                      */
    DDRC = 0x04;        /* set portC.2 as output  */
    /* Ext. Port                                  */
    DDRD = 0x00;        /* set portD as input     */

    /* LEDs                                       */
    PORTB = ~0x00;      /* all LEDs off           */
    /* Latch                                      */
    PORTC = 0x04;       /* make latch transparent */
    /* Ext. Port                                  */
    PORTD = 0x00;       /* disable PORTD pullups  */

    /* Hardware Initialization */
    USB_Init();
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void) {
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void) {
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void) {
    bool ConfigSuccess = true;

    ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void) {
    CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}
