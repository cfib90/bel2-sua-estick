#!/bin/bash
sens=./sensapp

usage() {
    echo "Usage: $0 <sin|tri|rnd> <p1> <p2> [<#samples>]"
    echo "sin: sine wave"
    echo "     p1    amplitude"
    echo "     p2    frequency"
    echo "tri: triangular wave"
    echo "     p1    amplitude"
    echo "     p2    frequency"
    echo "rnd: random values"
    echo "     p1    amplitude (range: 0 - amplitude)"
    echo "     p2    seed"
    echo "if samples=0, samples will be calculated for 1 period with 'sin' and 'tri'"
    exit -1
}

as() {
    echo "a($1 / sqrt(1 – $1^2))" | bc -l
}

cs() {
    echo "$1*s($2*($3/65536)*2*3.1415926)" | bc -l
}

ct() {
    t=$(echo "s(2*3.1415926*$2*$3/65536)" | bc -l)
    asin=$(echo "a($t / sqrt(1 - $t^2))" | bc -l)
    echo "(2*$1/3.1415926)*$asin" | bc -l
}

sin() {
    ampl=$1
    freq=$2
    if [ $3 -ne 0 ]
    then
        numsamples=$3
    else
        numsamples=`expr 65535 / $freq`
    fi

    # set ampl
    $sens -d /dev/ttyACM0 -a 11 -s $ampl
    # set freq
    $sens -d /dev/ttyACM0 -a 10 -s $freq
    # reset x
    $sens -d /dev/ttyACM0 -a 12 -s 0

    echo $numsamples
}

tri() {
    ampl=$1
    freq=$2
    if [ $3 -ne 0 ]
    then
        numsamples=$3
    else
        numsamples=`expr 65535 / $freq`
    fi

    # set ampl
    $sens -d /dev/ttyACM0 -a 21 -s $ampl
    # set freq
    $sens -d /dev/ttyACM0 -a 20 -s $freq
    # reset x
    $sens -d /dev/ttyACM0 -a 22 -s 0

    echo $numsamples
}

rnd() {
    ampl=$1
    seed=$2
    numsamples=$3

    # set freq
    $sens -d /dev/ttyACM0 -a 31 -s $1
    # reset x
    $sens -d /dev/ttyACM0 -a 32 -s $seed

    echo $numsamples
}

if [ $# -lt 3 ]
then
    usage $0
fi

num=0
numsamples=0

if [ $# -eq 4 ]
then
    num=$4
fi

case $1 in
    sin)
        numsamples=$(sin $2 $3 $num) ;;
    tri)
        numsamples=$(tri $2 $3 $num) ;;
    rnd)
        numsamples=$(rnd $2 $3 $num) ;;
    *)
        echo "unsupported operation"
        usage $0
        ;;
esac

rm -f $1.dat
rm -f $1.pdf

echo "$numsamples"
echo "#x y ok" > $1.dat

numsamples=`expr $numsamples + 1`
shift=0
addr=0

case $1 in
    sin ) 
          check="cs $2 $3"
          addr=10
          shift=32767;;
    tri ) check="ct $2 $3"
          addr=20
          shift=32767;;
    rnd ) check=echo
          addr=30 ;;
esac


for i in `seq 0 $numsamples`
do

    y=$($sens -d /dev/ttyACM0 -a $addr)

    real=$($check $i)

    if [ $? -ne 0 ]
    then
        echo "Failed"
        exit -1
    fi

    if [ `expr $i % 100` -eq 0 ]
    then
        echo "$i"
    fi
    echo "$i `expr $y - $shift` $real" >> $1.dat
done

gnuplot -e "set terminal pdf dashed; set output \"$1.pdf\"; \
            plot \"$1.dat\" using 1:2 title '$1: raw values as read' lt rgb \"blue\" w steps, \
            \"$1.dat\" using 1:2 title '$1: connected raw values' ls 1 lt rgb \"red\" w lines, \
            \"$1.dat\" using 1:3 title '$1: values as calculated on PC' ls 2 lc rgb \"magenta\" w lines;"

evince $1.pdf &

