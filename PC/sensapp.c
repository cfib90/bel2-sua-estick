/*
 * Author: Christian Fibich <fibich@technikum-wien.at>
 * Last change: 28.04.2015
 * eCDC host software demo
 */
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <termios.h>

#define MAX_MESSAGE_LENGTH 80
#define MY_ID  0
#define DEV_ID 1
#define MSG_ID 1337

char *progname = "(nul)";
int  fd_dev = -1;

/*
 * Free resources and exit
 */
void bailout(char *msg, int rv) {
    if(fd_dev != -1) {
        close(fd_dev);
    }
    (void) fprintf(stderr,"%s: %s\n", progname, msg);
    exit(rv);
}

void printUsage(FILE *fp, int rv) {
    (void) fprintf(fp,"Usage: %s [Options]\n" \
                      "    -d <path/to/device>  Device to use\n" \
                      "    -a <address>         Address to read from/write to [0 - 65535]\n" \
                      "    -s <data>            Data to write [0 - 65535]\n" \
                      "    -h                   Print this help\n",progname);
    exit(rv);
}

/*
 * Validate the parameters
 */
void check_params(unsigned int ssrc, unsigned int sdst, unsigned int sid,
                  unsigned int rsrc, unsigned int rdst, unsigned int rid) {
    if(ssrc != rdst) {
        bailout("Received message was not mine.",-1); 
    } else if(sdst != rsrc) {
        bailout("Received message was not from my device.",-1);
    } else if(sid != rid) {
        bailout("ID mismatch between sent and received message.",-1);
    }
}

int main(int argc, char *argv[]) {

    char opt,*tol;
    unsigned long addr,data;
    char do_write;
    char *dev = NULL;
    char smsg[MAX_MESSAGE_LENGTH+1],rmsg[MAX_MESSAGE_LENGTH+1];
    int wrote,rd,to_write;
    int verbose;
    int parse;
    unsigned int dst, src, id, rdata;
    char cmd;
    int  rv;
    struct termios my_serport;

    verbose = 0;
    
    progname = argv[0];
    do_write = 0;
    rd = 0;
    wrote = 0;

    addr = data = 0;

    if(argc == 1) {
        printUsage(stderr,-1);
    }

    /* Parse command line options */
    while( (opt = getopt(argc,argv,"d:a:s:hv")) != -1) {
        switch(opt) {
            case 'd':
            /* the serial device to be used */
            /* just a device file in /dev/ called /dev/ttyXXXy */
            /* e.g. /dev/ttyACM0 */
                dev = optarg;
            break;
            case 'a':
            /* the address to read from or write to */
                addr = strtoul(optarg,&tol,0);
                if(*tol != '\0') {
                    printUsage(stderr,-1);
                } else if(addr > 0xFFFF) {
                    (void) fprintf(stderr,"Addr (%ld) is out of range 0 - 65535.\n",addr);
                    printUsage(stderr,-1);
                }
                
            break;
            case 's':
            /* if this parameter is passed, write to specified addr */
            /* this is the data to be written */
                do_write = 1;
                data = strtoul(optarg,&tol,0);
                if(*tol != '\0') {
                    printUsage(stderr,-1);
                } else if(data > 0xFFFF) {
                    (void) fprintf(stderr,"Data (%ld) is out of range 0 - 65535.\n",data);
                    printUsage(stderr,-1);
                }
            break;
            case 'h':
                printUsage(stdout,0);
            break;
            case 'v':
                verbose = 1;
            break;
            default:
                printUsage(stderr,-1);
        }
    }

    if(dev == NULL) {
        (void) fprintf(stderr,"-d is a required parameter.\n");
        printUsage(stderr,-1);
    }

    /* (try to) open the serial device, set some options */
    /* O_RDWR   - we want to read and write from this device */
    /* O_NOCTTY - this will not become a control TTY */
    /* O_SYNC   - synchronized transmission */

    fd_dev = open(dev,O_RDWR | O_NOCTTY | O_SYNC);

    if(fd_dev < 0) {
        perror(progname);
        bailout("Could not open device.",-1);
    }

    /**************************************************************************
     * Serial port configuration                                              *
     *************************************************************************/

    /* we need to set some parameters for a serial port */
    /* for an explanation, see 'man 3 termios' */

    memset(&my_serport,0,sizeof(my_serport));

    /* The flags and fields in c_iflag control parameters usually associated with asynchronous serial data transmission. */
    /* If this bit is set, break conditions are ignored.
     * A break condition is defined in the context of asynchronous serial data
     * transmission as a series of zero-value bits longer than a single byte. */
    my_serport.c_iflag &= ~IGNBRK;

    /* The flags in c_lflag generally control higher-level aspects of input
       processing than the input modes flags in c_iflag
       such as echoing, signals, and the choice of canonical or noncanonical input.*/
    my_serport.c_lflag = 0;          /* no canonical processing: read() returns only after timeout, no local echo */

    /* The flags and fields in c_oflag control how output characters are
       translated and padded for display. */
    my_serport.c_oflag = 0;                /* no remapping, no delays */

    /* In noncanonical input mode, the special editing characters such as ERASE
       and KILL are ignored. The system facilities for the user to edit input 
       are disabled in noncanonical mode, so that all input characters
       (unless they are special for signal or flow-control purposes)
       are passed to the application program exactly as typed.
       It is up to the application program to give the user ways to edit the
       input, if appropriate. */

    my_serport.c_cc[VMIN]  = 0;            /* specifies the minimum number of bytes that must be available
                                            * in the input queue in order for read to return: 0 = read doesn't block */
    my_serport.c_cc[VTIME] = 10;           /* specifies how long to wait for input before returning,
                                            * in units of 0.1 seconds: 10 = 1 seconds read timeout */

    my_serport.c_iflag &= ~(IXON | IXOFF | IXANY); /* shut off xon/xoff ctrl */


    /* The flags and fields in c_cflag control parameters usually associated with asynchronous serial data transmission */
    /* CLOCAL: If this bit is set, it indicates that the terminal is 
     *         connected “locally” and that the modem status lines (such as carrier detect) should be ignored.
     * CREAD:  If this bit is set, input can be read from the terminal.
     *         Otherwise, input is discarded when it arrives. */
    my_serport.c_cflag |= (CLOCAL | CREAD);

    /*shut off parity */
    /* PARENB: If this bit is not set, no parity bit is added to output characters,
     *         and input characters are not checked for correct parity. 
     * PARODD: This bit is only useful if PARENB is set.
     *         If PARODD is set, odd parity is used, otherwise even parity is used. */
    my_serport.c_cflag &= ~(PARENB | PARODD);

    /* If this bit is set, two stop bits are used. Otherwise, only one stop bit is used. */
    my_serport.c_cflag &= ~CSTOPB;    /* 1 stop bit */

     /* disable hardware flow control */
    my_serport.c_cflag &= ~CRTSCTS;  

    /* set serial port parameters */
    if(tcsetattr(fd_dev,TCSANOW,&my_serport) != 0) {
        perror(progname);
        bailout("Could not set serial port info.",-1);
    }

    /* clear data in input/output queues */
    if(tcflush(fd_dev, TCIOFLUSH) != 0) {
        perror(progname);
        bailout("Could not flush serial port.",-1);
    }

    /**************************************************************************
     * Message generation                                                     *
     *************************************************************************/

    /* generate the message to be sent over serial */
    if(do_write) {
        snprintf(smsg,MAX_MESSAGE_LENGTH,"<%u,%u,%u,w,%u,%u>\n",(unsigned short)MY_ID,(unsigned short)DEV_ID,(unsigned short)MSG_ID,(unsigned short)addr,(unsigned short)data);
    } else {
        snprintf(smsg,MAX_MESSAGE_LENGTH,"<%u,%u,%u,r,%u>\n",(unsigned short)MY_ID,(unsigned short)DEV_ID,(unsigned short)MSG_ID,(unsigned short)addr);
    }
    
    if(verbose) {
        (void) fprintf(stdout,"%s: Sending message: %s\n",progname,smsg);
    }

    /**************************************************************************
     * Sending message over serial port                                       *
     *************************************************************************/

    to_write = strlen(smsg);

    /* push the message out */
    while(to_write > 0) {
        int rv;
        rv = write(fd_dev,smsg+wrote,to_write);
        if(rv < 0) {
            bailout("Error writing to device.",-1);
        }
        wrote    += rv;
        to_write -= rv;
    }

    if(verbose) {
        (void) fprintf(stdout,"Characters written: %d\n",wrote);
    }

    /* wait until all data has been passed to the underlying hardware */
    if(tcdrain(fd_dev) != 0) {
        perror(progname);
        bailout("Could not drain serial port.",-1);
    }


    /**************************************************************************
     * Receiving data over serial port                                        *
     *************************************************************************/

    /* try to fill the buffer, but not read any more than MAX_MESSAGE_LENGTH bytes*/
    do {
        int rv;
        rv = read(fd_dev,rmsg+rd,1);
        if(rv < 0) {
            perror(progname);
            bailout("Error reading from device.",-1);
        } else if(rv == 0) {
            /* device closed */
            break;
        } else if(rmsg[rd] == '\n') {
            /* line ended */
            break;
        }
        rd += rv;
    } while(rd < MAX_MESSAGE_LENGTH);

    if(rmsg[rd] != '\n') {
        bailout("Error receiving message.: Too long for buffer or connection closed.",-1);
    }

    /* done with the serial port */
    close(fd_dev);
    fd_dev = -1;

    rmsg[rd] = '\0';

    if(verbose) {
        (void) fprintf(stdout,"Received message: %s\n",rmsg);
    }

    /**************************************************************************
     * Parsing the received data                                              *
     **************************************************************************/
 
    /* check which message type */
    parse = sscanf(rmsg,"<%u,%u,%u,%c,%u>", &src, &dst, &id, &cmd, &rdata);
    if(parse >= 4) {
        check_params(MY_ID,DEV_ID,MSG_ID,src,dst,id);
    }

    if(verbose)
        (void) fprintf(stdout,"do_write=%d, Parse=%d.\n",do_write,parse);

    if(do_write && parse == 5) {
        if(verbose)
            (void) fprintf(stdout,"Write %s with data %u.\n--Fin--\n",(cmd == 'a') ? "succeeded" : "failed",rdata);
        rv = (cmd == 'a') ? 0 : -1;
    } else if (do_write == 0 && parse == 4){
        if(verbose)
            (void) fprintf(stdout,"Read failed.\n--Fin--\n");
        rv = -1;
    } else if (do_write == 0 && parse == 5){
        if(verbose)
            (void) fprintf(stdout,"Read returned %u.\n--Fin--\n",rdata);
        else
            fprintf(stdout,"%u",rdata);
        rv = 0;
    } else {
        bailout("Error parsing write return message.\n--Fin--\n",-1);
    }

    return rv;
}