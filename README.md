# README #

## Software you need ##
+ gcc
+ gnuplot
+ bc
+ For compiling the e-Stick firmware:
    + gcc-avr
    + avr-libc
    + avrdude

## e-Stick firmware ##
Located in directory `eCDC`.

Implements the base feature set as specified in BEL2 SUA Class Notes.

### Building (optional) ###

Execute `make` in directory `eCDC`.

### Flashing e-Stick (optional) ###

1. Connect e-Stick to USB port while pressing the tiny button
2. Execute `make estick-install` in directory `eCDC`

### Using the e-Stick with eCDC firmware ###

1. Connect e-Stick to USB port _without_ pressing the tiny button
2. (only if you are in a VM): Connect the new USB device to the VM
3. Read and write to the e-Stick's device file (something like /dev/ttyACM0)
   This is a virtual serial port. Refer to PC software on how configure it.

## PC software ##

Located in directory `PC`.

+ `sensapp` can be used to send 1 command to the eStick
+ `senstest.sh` can be used to test the firmware functionality by generating sine, tri, rnd.

### Building ###

Execute `make` in directory `PC`.